package com.app;



import java.awt.*;
import java.util.ArrayList;

/**
 * Klasa odpowiadajaca za plansze, ktora poki co testuje
 */
public class ServerBoard  {

    private Tile[][] board;
    private int board_size_x, board_size_y;
    private int triangle_size;
    private ArrayList<Point> UpperTriangle;
    private ArrayList<Point> LowerTriangle;
    private ArrayList<Point> UpperLeftTriangle;
    private ArrayList<Point> LowerLeftTriangle;
    private ArrayList<Point> UpperRightTriangle;
    private ArrayList<Point> LowerRightTriangle;
    private ArrayList<Point>[] destination_triangles;
    public ServerBoard(int triangle_size) {
        this.triangle_size = triangle_size;
        generateBoard();
    }

    private void generateBoard() {
        board_size_x = triangle_size * 4 + 5;
        board_size_y = triangle_size * 4 + 5;
        board = new Tile[board_size_y][board_size_x];
        UpperTriangle= new ArrayList<>();
        LowerTriangle= new ArrayList<>();
        UpperLeftTriangle= new ArrayList<>();
        LowerLeftTriangle= new ArrayList<>();
        UpperRightTriangle= new ArrayList<>();
        LowerRightTriangle= new ArrayList<>();
        destination_triangles = new ArrayList[6];
        destination_triangles[3] = LowerTriangle;
        destination_triangles[5] = LowerLeftTriangle;
        destination_triangles[4] = UpperLeftTriangle;
        destination_triangles[2] = UpperTriangle;
        destination_triangles[0] = UpperRightTriangle;
        destination_triangles[1] = LowerRightTriangle;
        createHexagramBoard(triangle_size);
    }

    private void createHexagramBoard(int triangle_size) {
        int marginX = 2;
        int marginY = 2;
        createUpperBigTriangle(triangle_size, marginX, marginY);
        createLowerBigTriangle(triangle_size, marginX, marginY);

        createLowerSmallTriangle(triangle_size, marginX, marginY);
        createUpperSmallTriangle(triangle_size, marginX, marginY);
        createUpperLeftSmallTriangle(triangle_size, marginX, marginY);
        createUpperRightSmallTriangle(triangle_size, marginX, marginY);
        createLowerLeftSmallTriangle(triangle_size, marginX, marginY);
        createLowerRightSmallTriangle(triangle_size, marginX, marginY);

    }

    private void createUpperBigTriangle(int triangle_size, int marginX, int marginY) {
        int board_size = triangle_size * 4 + 5;
        int triangle_constant = 3 * triangle_size + 4;
        for (int i = 0; i< board_size_x; i++) {
            for (int j = 0; j < board_size_y; j++) {
                if (j < board_size - marginY - triangle_size &&
                        i < board_size - marginX - triangle_size &&
                        i + j >= triangle_constant) {
                    board[j][i] = new Tile(-1);
                }
            }
        }
    }

    private void createLowerBigTriangle(int triangle_size, int marginX, int marginY) {
        int board_size = triangle_size * 4 + 5;
        int triangle_constant = triangle_size * 5 + 4;
        for (int i = 0; i< board_size_x; i++) {
            for (int j = 0; j < board_size_y; j++) {
                if ( j >= marginY + triangle_size &&
                        i >= marginX + triangle_size && i+j <= triangle_constant) {
                    board[j][i] = new Tile(-1);
                }
            }
        }
    }


    private void createUpperSmallTriangle(int triangle_size, int marginX, int marginY) {
        int board_size = triangle_size * 4 + 5;
        int triangle_constant = 3 * triangle_size + 4;
        for (int i = 0; i< board_size_x; i++) {
            for (int j = 0; j < board_size_y; j++) {
                if (j < triangle_size + marginY &&
                        i < board_size - marginX - triangle_size && i + j >= triangle_constant) {
                    board[j][i] = new Tile(3);
                    UpperTriangle.add(new Point(j,i));
                }
            }
        }
    }

    private void createUpperLeftSmallTriangle(int triangle_size, int marginX, int marginY) {
        int board_size = triangle_size * 4 + 5;
        int triangle_constant = 3 * triangle_size + 4;
        for (int i = 0; i< board_size_x; i++) {
            for (int j = 0; j < board_size_y; j++) {
                if (j >= marginY + triangle_size &&
                        i >= triangle_size + marginX && i + j < triangle_constant) {
                    board[j][i] = new Tile(1);
                    UpperLeftTriangle.add(new Point(j,i));
                }
            }
        }
    }

    private void createUpperRightSmallTriangle(int triangle_size, int marginX, int marginY) {
        int board_size = triangle_size * 4 + 5;
        int triangle_constant = 5 * triangle_size + 4;
        for (int i = 0; i< board_size_x; i++) {
            for (int j = 0; j < board_size_y; j++) {
                if (j >= marginY + triangle_size &&
                        i >= board_size - marginX - triangle_size && i + j <= triangle_constant) {
                    board[j][i] = new Tile(5);
                    UpperRightTriangle.add(new Point(j,i));
                }
            }
        }
    }


    private void createLowerSmallTriangle(int triangle_size, int marginX, int marginY) {
        int board_size = triangle_size * 4 + 5;
        int triangle_constant = triangle_size * 5 + 4;
        for (int i = 0; i< board_size_x; i++) {
            for (int j = 0; j < board_size_y; j++) {
                if ( j >= board_size - marginY - triangle_size &&
                        i >= marginX + triangle_size && i+j <= triangle_constant) {
                    board[j][i] = new Tile(2);
                    LowerTriangle.add(new Point(j,i));
                }
            }
        }
    }

    private void createLowerLeftSmallTriangle(int triangle_size, int marginX, int marginY) {
        int board_size = triangle_size * 4 + 5;
        int triangle_constant = 3 * triangle_size + 4;
        for (int i = 0; i< board_size_x; i++) {
            for (int j = 0; j < board_size_y; j++) {
                if ( j < board_size - marginY - triangle_size &&
                        i < marginX + triangle_size && i+j >= triangle_constant) {
                    board[j][i] = new Tile(0);
                    LowerLeftTriangle.add(new Point(j,i));
                }
            }
        }
    }

    private void createLowerRightSmallTriangle(int triangle_size, int marginX, int marginY) {
        int board_size = triangle_size * 4 + 5;
        int triangle_constatnt = triangle_size * 5 + 4;
        for (int i = 0; i< board_size_x; i++) {
            for (int j = 0; j < board_size_y; j++) {
                if ( j < board_size - marginY - triangle_size &&
                        i < board_size - marginX - triangle_size && i+j > triangle_constatnt) {
                    board[j][i] = new Tile(4);
                    LowerRightTriangle.add(new Point(j,i));
                }
            }
        }
    }



    public Tile[][] getBoard() {
        return board;
    }

    public int getBoardSizeX() {
        return board_size_x;
    }

    public int getBoardSizeY() {
        return board_size_y;
    }

    public int getTitleOwner(int x,int y){
        return board[x][y]==null ? -2 : board[x][y].getOwner();
    }

    public void setTitleOwner(int x,int y,int player){
        board[x][y].setOwner(player);
    }

    public void makeMove(int x, int y, int desc_x, int desc_y) {
        int owner=board[x][y].getOwner();
        this.setTitleOwner(x,y,-1);
        this.setTitleOwner(desc_x,desc_y,owner);
    }

    public ArrayList<Point> getDestinationTriangle(int player) {
        return destination_triangles[player];
    }

}
