package com.app.Game;

import com.app.Player.PlayerInterface;

public interface Judge {

        /**
        private boolean checkMove(int ox,int oy,int nx,int ny);
        private int checkBoardState();
        private boolean isThatPlayerChecker(int player);
         **/
        public boolean makeMove(PlayerInterface player, int ox, int oy, int nx, int ny);
        public String giveCheckerMoves(PlayerInterface player,int x,int y);
        public String giveBotMoves(PlayerInterface player, int x, int y);
        public boolean checkBoardState(PlayerInterface player);

}
