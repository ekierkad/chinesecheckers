package com.app.Player;

import java.util.Iterator;

public interface PlayerInterface {
	public String getName();
	public Iterator getIterator();

    void setName(String args);

	String getState();
	void setState(String state);
	boolean getReady();
	int getPlayerId();
}
