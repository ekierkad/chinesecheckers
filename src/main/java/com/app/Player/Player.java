package com.app.Player;

import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;

public class Player implements PlayerInterface {
	private String name=null;
	ArrayList<Point> destinationTriangle;
	private int playerId;
	String state;
	boolean ready=false;
	public Player() {

	};

	public Player(String name){
		this.name=name;
	}
	@Override
	public String getName() {
		return name;
	}
	public void setPlayerId(int id){
		playerId=id;
	}
	public int getPlayerId(){
		return playerId;
	}
	public Iterator getIterator(){
		return destinationTriangle.iterator();
	}
	public void setDestinationTriangle(ArrayList<Point> destinationTriangle) {
		this.destinationTriangle = destinationTriangle;
	}
	public void setName(String name){this.name=name;}
	public String getState(){return state;}
	public void setState(String state){this.state=state;}

	public void setReady(boolean b) {
		ready=b;
	}
	public boolean getReady(){
		return ready;
	}
}
