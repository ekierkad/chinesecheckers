package com.app.Player;

import com.app.Game.ClassicJudge;
import com.app.Game.Judge;
import com.app.ServerBoard;
import com.app.Tile;

import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;

public class BotPlayer implements PlayerInterface {

    private String name="bot";
    private ArrayList<Point> destinationTriangle;
    private int playerId;
    private String state;
    private ServerBoard serverBoard;
    private Tile[][] board;
    private Judge judge;


    public boolean getReady(){
        return true;
    }

    public String makeBestMove() {
        ArrayList<BotMove> moves = new ArrayList<>();
        for (int i = 0; i < serverBoard.getBoardSizeX(); i++) {
            for (int j = 0; j < serverBoard.getBoardSizeY(); j++) {
                if (serverBoard.getTitleOwner(i, j) == playerId) {
                    createBotMovesForTile(i, j, moves);
                }
            }
        }
        return chooseBestMove(moves);
    }

    private void createBotMovesForTile(int oldX, int oldY, ArrayList<BotMove> moves) {
        String moves_string = judge.giveBotMoves(this, oldX, oldY);
        String moves_array[] = moves_string.split(" ");
        int number_of_moves = Integer.parseInt(moves_array[0]);
        for (int i = 0; i<number_of_moves; i++) {
            int newX = Integer.parseInt(moves_array[1 + i*2]);
            int newY = Integer.parseInt(moves_array[2 + i*2]);
            Tile[][] board_copy = copyBoard();
            BotMove botMove = new BotMove(playerId, destinationTriangle, board_copy);
            botMove.generateBoardAfterMove(oldX, oldY, newX, newY);
            botMove.evaluateBoard();
            moves.add(botMove);
        }
    }

    private Tile[][] copyBoard() {
        Tile[][] board_copy = new Tile[serverBoard.getBoardSizeY()][serverBoard.getBoardSizeX()];
        for (int i=0; i<serverBoard.getBoardSizeY(); i++) {
            for (int j=0; j<serverBoard.getBoardSizeX(); j++) {
                board_copy[i][j] = board[i][j];
            }
        }
        return board_copy;
    }

    private String chooseBestMove(ArrayList<BotMove> moves) {
        BotMove botMove = new BotMove(playerId, destinationTriangle, board);
        botMove.evaluateBoard();
        int best_move_value = botMove.getMoveValue(); //domyslna wartosc to wartosc kiedy nie wykonalismy zadnego ruchu
        BotMove best_move = null;
        for (BotMove move : moves) {
            if (move.getMoveValue() <= best_move_value) {
                best_move = move;
                best_move_value = best_move.getMoveValue();
            }
        }
        if (best_move == null) {
            return "SKIP";
        }
        else {
            String arr[]=best_move.getMoveString().split(" ");
            int fromX = Integer.parseInt(arr[1]);
            int fromY = Integer.parseInt(arr[2]);
            int toX = Integer.parseInt(arr[3]);
            int toY = Integer.parseInt(arr[4]);
            serverBoard.makeMove(fromX, fromY, toX, toY);
            return best_move.getMoveString();
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Iterator getIterator() {
        return destinationTriangle.iterator();
    }

    @Override
    public void setName(String args) {
        this.name = args;
    }

    @Override
    public String getState() {
        return state;
    }

    @Override
    public void setState(String state) {
        this.state = state;
    }

    public void setServerBoard(ServerBoard board) {
        this.serverBoard = board;
        this.board = this.serverBoard.getBoard();
    }

    public void setJudge(Judge judge) {
        this.judge = judge;
    }

    public void setDestinationTriangle(ArrayList<Point> destinationTriangle) {
        this.destinationTriangle = destinationTriangle;
    }

    public void setPlayerId(int id){
        playerId=id;
    }
    public int getPlayerId(){
        return playerId;
    }
}
