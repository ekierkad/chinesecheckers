package com.app.Player;

import com.app.Tile;

import java.awt.*;
import java.util.ArrayList;

public class BotMove {

    private Tile[][] boardAfterMove;
    private int moveValue;
    private String moveString;
    private ArrayList<Point> destinationTriangle;
    private int playerId;
    private Point[] cornerPoints;
    private Point destinationPoint;

    public BotMove(int playerId, ArrayList<Point> destinationTriangle, Tile[][] board) {
        this.playerId = playerId;
        this.destinationTriangle = destinationTriangle;
        this.boardAfterMove = board;

        cornerPoints = new Point[6];
        cornerPoints[0] = new Point(14, 2);
        cornerPoints[1] = new Point(18, 6);
        cornerPoints[2] = new Point(14, 14);
        cornerPoints[3] = new Point(6, 18);
        cornerPoints[4] = new Point(2, 14);
        cornerPoints[5] = new Point(6, 6);
        //Znajdujemy wlasciwy dla gracza corner_point
        for (Point corner_point : cornerPoints) {
            for (Point point : destinationTriangle) {
                if (corner_point.x == point.x && corner_point.y == point.y) {
                    destinationPoint = point;
                }
            }
        }
    }

    public void generateBoardAfterMove(int fromX, int fromY, int toX, int toY) {
        boardAfterMove[fromX][fromY] = null;
        boardAfterMove[toX][toY] = new Tile(playerId);
        this.moveString = "MOVE_DONE "+fromX+" "+fromY+" "+toX+" "+toY;
    }

    public void evaluateBoard() {
        int sum_of_distances = 0;
        int destX = destinationPoint.x;
        int destY = destinationPoint.y;
        for (int i=0; i<boardAfterMove.length; i++) {
            for (int j=0; j<boardAfterMove.length; j++) {
                if(boardAfterMove[i][j] != null && boardAfterMove[i][j].getOwner() == playerId) {
                    int distance = (Math.abs(destX - i) + Math.abs(destX + destY - i - j) + Math.abs(destY - j)) / 2;
                    //System.out.println("wyliczony dystans dla punktu "+i+" "+j+" :"+distance);
                    sum_of_distances += distance;
                }
            }
        }
        if (numberOfPawnsInDestinationTriangle() == 9) {
            sum_of_distances -= 30;
            Point newDestinationPoint = getNotOcuppiedDestinationPoint();
            int dX = newDestinationPoint.x;
            int dY = newDestinationPoint.y;
            Point pawnPoint = getPawnOutsideDestinationTriangle();
            int pX = pawnPoint.x;
            int pY = pawnPoint.y;
            int distance = (Math.abs(dX - pX) + Math.abs(dX + dY - pX - pY) + Math.abs(dY - pY)) / 2;
            sum_of_distances += distance;
        }

        if (numberOfPawnsInDestinationTriangle()==10) {
            sum_of_distances = -1000;
        }
        moveValue = sum_of_distances;
    }

    private Point getNotOcuppiedDestinationPoint() {
        for (Point point : destinationTriangle) {
            if (boardAfterMove[point.x][point.y] != null && boardAfterMove[point.x][point.y].getOwner() != playerId) {
                return point;
            }
        }
        return destinationPoint;
    }

    private Point getPawnOutsideDestinationTriangle() {
        boolean is_in_triangle = false;
        for (int i=0; i< boardAfterMove.length; i++) {
            for (int j=0; j<boardAfterMove.length; j++) {
                if (boardAfterMove[i][j] != null && boardAfterMove[i][j].getOwner() == playerId) {
                    for (Point point : destinationTriangle) {
                        if (i==point.x && j==point.y) {
                            is_in_triangle = true;
                        }
                    }
                    if (!is_in_triangle) {
                        return new Point(i, j);
                    }
                }
            }
        }
        return getFarthestPoint();
    }

    private Point getFarthestPoint() {
        int biggest_distance = 0;
        Point farthest_point = new Point(0,0);
        for (int i=0; i< boardAfterMove.length; i++) {
            for (int j = 0; j < boardAfterMove.length; j++) {
                if (boardAfterMove[i][j] != null && boardAfterMove[i][j].getOwner() == playerId) {
                    int destX = destinationPoint.x;
                    int destY = destinationPoint.y;
                    int distance = (Math.abs(destX - i) + Math.abs(destX + destY - i - j) + Math.abs(destY - j)) / 2;
                    if (distance > biggest_distance) {
                        biggest_distance = distance;
                        farthest_point = new Point(i, j);
                    }
                }
            }
        }
        return farthest_point;
    }

    private int numberOfPawnsInDestinationTriangle() {
        int result = 0;
        for (Point point : destinationTriangle) {
            for (int i=0; i< boardAfterMove.length; i++) {
                for (int j=0; j<boardAfterMove.length; j++) {
                    if (boardAfterMove[i][j] != null && boardAfterMove[i][j].getOwner() == playerId && i == point.x && j==point.y) {
                        result++;
                    }
                }
            }
        }
        return result;
    }

    public String getMoveString() {
        return moveString;
    }

    public int getMoveValue() {
        return moveValue;
    }

}
