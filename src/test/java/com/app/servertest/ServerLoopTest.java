package com.app.servertest;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import com.app.server.ServerLoop;
import org.junit.Test;
import org.mockito.Mockito;

public class ServerLoopTest {

	@Test(timeout=10000) //ten seconds or die
	public void socketTest() {
		Socket socket;
		try {
			socket = new Socket("127.0.0.1",2137);
			PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
		    BufferedReader in = new BufferedReader(
		        new InputStreamReader(socket.getInputStream()));
		    out.println("test");
		    waitForInfo(in);
		    assertEquals("test",in.readLine());
		    socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}


	}

	private void waitForInfo(BufferedReader in) throws IOException {
		while(in.ready()==false) {
			continue;
		}
	}
}
