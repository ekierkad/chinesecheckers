package com.app.servertest;

import com.app.Game.Game;
import com.app.Player.Player;
import com.app.server.Translator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

public class TranslatorTest {

    Translator translator;
    Game game;
    Player player;
    @Before
    public void setup(){
        game=new Game();
        translator=new Translator(game);
        player=new Player();
        player.setPlayerId(1);
        player.setDestinationTriangle(new ArrayList<>());
    }

    @Test
    public void TranslatorNickTest(){
        Assert.assertEquals(translator.decodeMessage(player,"NICK wojtek"),"NICK_ACCEPTED wojtek");
    }

    @Test
    public void TranslatorMovesCheck(){

        Assert.assertEquals(translator.decodeMessage(player,"MOVES 7 8"),"AVAILABLE_MOVES 0");
    }
    @Test
    public void  TranslatorMoveCheck(){
        Assert.assertEquals(translator.decodeMessage(player,"MOVE 7 8 7 9"),"MOVE_DONE FAILURE");
    }

    @Test
    public void TranslatorJoinSlot(){
        Assert.assertEquals(translator.decodeMessage(player,"JOIN_SLOT 1"),"FAIL");
    }
    @Test
    public void TranslatorLeaveSlot(){
        translator.decodeMessage(player,"JOIN_SLOT 1");
        Assert.assertEquals(translator.decodeMessage(player,"LEAVESLOT 1"),"LEAVESLOT SUCCESS");
    }
    @Test
    public void TranslatorPlayerReady(){
        translator.decodeMessage(player,"JOIN_SLOT 1");
        Assert.assertEquals(translator.decodeMessage(player,"PLAYER_READY"),"SUCCESS");

    }
    @Test
    public void TranslatorPlayerNotReady(){
        translator.decodeMessage(player,"JOIN_SLOT 1");
        Assert.assertEquals(translator.decodeMessage(player,"PLAYER_NOT_READY"),"SUCCESS");

    }

    @Test
    public void TranslatorSkipTurn(){
        translator.decodeMessage(player,"JOIN_SLOT 1");
        Assert.assertEquals(translator.decodeMessage(player,"SKIP_TURN 1"),"FAIL");

    }
    @Test
    public void TranslatorAddBot(){
        Assert.assertEquals(translator.decodeMessage(player,"ADD_BOT 1"),"FAIL");
    }
    @Test
    public void TranslatorDeleteBot(){
        translator.decodeMessage(player,"ADD_BOT 1");
        Assert.assertEquals(translator.decodeMessage(player,"REMOVE_BOT 1"),"FAIL");

    }
    @Test
    public void TranslatorPlayerHasLeft(){
        translator.decodeMessage(player,"JOIN_SLOT 1");
        Assert.assertEquals(translator.decodeMessage(player,"PLAYER_HAS_LEFT"),"FAIL");
    }

}
