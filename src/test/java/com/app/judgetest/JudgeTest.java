package com.app.judgetest;

import com.app.Game.ClassicJudge;
import com.app.Game.Judge;
import com.app.Player.Player;
import com.app.ServerBoard;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;

public class JudgeTest {

    @Test
    public void JudgeDFSTest(){
        ServerBoard board=new ServerBoard(4);
        ClassicJudge judge=new ClassicJudge(board);
        Player player=new Player();
        player.setPlayerId(1);
        player.setDestinationTriangle(board.getDestinationTriangle(1));
        Assert.assertEquals("2 7 9 8 8",judge.giveCheckerMoves(player,7,8));
        Assert.assertEquals("0",judge.giveCheckerMoves(player,20,20));
    }
    @Test
    public void JudgeMakeMoveTest(){
            ServerBoard board=new ServerBoard(4);
            ClassicJudge judge=new ClassicJudge(board);
            Player player=new Player();
            player.setPlayerId(1);
            player.setDestinationTriangle(board.getDestinationTriangle(1));
            Assert.assertEquals("2 7 9 8 8",judge.giveCheckerMoves(player,7,8));
            Assert.assertTrue(judge.makeMove(player,7,8,7,9));
            Assert.assertEquals("0",judge.giveCheckerMoves(player,20,20));
            Assert.assertFalse(judge.makeMove(player,7,8,7,9));


    }
    @Test
    public void JudgeGetBotMovesTest(){
        ServerBoard board=new ServerBoard(4);
        ClassicJudge judge=new ClassicJudge(board);
        Player player=new Player();
        player.setPlayerId(1);
        player.setDestinationTriangle(board.getDestinationTriangle(1));
        Assert.assertEquals("2 7 9 8 8",judge.giveBotMoves(player,7,8));
        Assert.assertTrue(judge.makeMove(player,7,8,7,9));


    }
    @Test
    public void JudgeTriangleDFSTest(){
        ServerBoard board=new ServerBoard(4);
        ClassicJudge judge=new ClassicJudge(board);
        Player player=new Player();
        player.setPlayerId(1);
        player.setDestinationTriangle(board.getDestinationTriangle(1));
        Iterator<Point>it= player.getIterator();

        Point p=it.next();
        if(it.hasNext()){
            board.setTitleOwner(p.x,p.y,player.getPlayerId());
        }
        Assert.assertEquals("0",judge.giveCheckerMoves(player,p.x,p.y));
    }

}
