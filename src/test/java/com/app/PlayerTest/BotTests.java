package com.app.PlayerTest;

import com.app.Game.ClassicJudge;
import com.app.Player.BotMove;
import com.app.Player.BotPlayer;
import com.app.ServerBoard;
import org.junit.Assert;
import org.junit.Test;

public class BotTests {

    @Test
    public void testBotPlayer() {
        BotPlayer botPlayer = new BotPlayer();
        ServerBoard board = new ServerBoard(4);
        botPlayer.setServerBoard(board);
        botPlayer.setName("bot");
        botPlayer.setJudge(new ClassicJudge(board));
        botPlayer.setPlayerId(4);
        botPlayer.setDestinationTriangle(board.getDestinationTriangle(4));
        botPlayer.setState("READY");

        Assert.assertTrue(botPlayer.getReady());
        Assert.assertEquals("bot",botPlayer.getName());

        Assert.assertEquals(4, botPlayer.getPlayerId());
        Assert.assertNotNull(botPlayer.getIterator());
        Assert.assertEquals("READY",botPlayer.getState());
    }

    @Test
    public void testBestMove() {
        BotPlayer botPlayer = new BotPlayer();
        ServerBoard board = new ServerBoard(4);
        botPlayer.setServerBoard(board);
        botPlayer.setJudge(new ClassicJudge(board));
        botPlayer.setPlayerId(0);
        botPlayer.setDestinationTriangle(board.getDestinationTriangle(0));

        String best_move = botPlayer.makeBestMove();
        Assert.assertEquals("MOVE_DONE 14 4 14 6",best_move);
    }

    @Test
    public void testBotMove() {
        BotPlayer botPlayer = new BotPlayer();
        ServerBoard board = new ServerBoard(4);
        botPlayer.setServerBoard(board);
        botPlayer.setJudge(new ClassicJudge(board));
        botPlayer.setPlayerId(1);
        botPlayer.setDestinationTriangle(board.getDestinationTriangle(1));

        BotMove botMove = new BotMove(1, board.getDestinationTriangle(1), board.getBoard());
        botMove.evaluateBoard();
        Assert.assertEquals(140, botMove.getMoveValue());

        botMove.generateBoardAfterMove(9, 6, 10, 6);
        botMove.evaluateBoard();
        Assert.assertEquals(139, botMove.getMoveValue());
        Assert.assertEquals("MOVE_DONE 9 6 10 6",botMove.getMoveString());
    }

    @Test
    public void testBotMoveWithNinePawnsInDestinationTriangle() {
        BotPlayer botPlayer = new BotPlayer();
        ServerBoard board = new ServerBoard(4);
        botPlayer.setServerBoard(board);
        botPlayer.setJudge(new ClassicJudge(board));
        botPlayer.setPlayerId(4);
        botPlayer.setDestinationTriangle(board.getDestinationTriangle(1));

        BotMove botMove = new BotMove(4, board.getDestinationTriangle(1), board.getBoard());
        botMove.generateBoardAfterMove(14,11, 11, 9);
        botMove.evaluateBoard();
        Assert.assertEquals(3, botMove.getMoveValue());
        Assert.assertEquals("MOVE_DONE 14 11 11 9", botMove.getMoveString());
    }
}
