package com.app.PlayerTest;

import com.app.Player.Player;
import org.junit.Assert;
import org.junit.Test;

import java.awt.*;
import java.util.ArrayList;

public class PlayerTests {
    @Test
    public void PlayerTest(){
        Player player=new Player();
        player.setState("Playing");
        Assert.assertEquals("Playing",player.getState());
        player.setPlayerId(1);
        Assert.assertEquals(1,player.getPlayerId());
        player.setName("zbysiek");
        Assert.assertEquals("zbysiek",player.getName());
        player.setReady(true);
        Assert.assertTrue(player.getReady());
        ArrayList<Point> array=new ArrayList<>();
        player.setDestinationTriangle(array);
        Assert.assertNotNull(player.getIterator());
    }
}
